package main

import (
	"fmt"
	"math"
)

func main() {
	var amount, period, rate int64

	fmt.Printf("Enter amount of loan:\n")
	fmt.Scanf("%d\n", &amount)

	fmt.Printf("Enter period in years:\n")
	fmt.Scanf("%d\n", &period)

	fmt.Printf("Enter interest rate:\n")
	fmt.Scanf("%d\n", &rate)

	AmortizationCalc(float64(amount), float64(period), float64(rate))

}

func AmortizationCalc(amount, period, rate float64) (result float64) {

	var payment, interest, principal float64
	payment = (amount * (rate / 1200) * math.Pow((1+rate/1200), 12*period)) / (math.Pow((1+rate/1200), 12*period) - 1)
	result = 12 * period * payment

	fmt.Printf("|#\t |Payment\t |Principal\t |Interest\t |Balance\n")
	for i := 1; i <= 12*int(period); i++ {
		interest = (rate / 1200) * amount
		principal = payment - interest
		amount -= principal

		fmt.Printf("|%-6d\t |%-6.2f\t |%-6.2f\t |%-6.2f\t |%-6.2f\n", i, payment, principal, interest, amount)
	}

	return
}
